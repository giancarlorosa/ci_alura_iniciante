<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produtos extends CI_Controller {

	public function index()
	{
		$this->load->model('produtos_model');
		$produtos = $this->produtos_model->buscaTodos();

		$dados = array('produtos' => $produtos);
		$this->load->template('produtos/index', $dados);		
	}

	public function formulario ()
	{
		autoriza();
		$this->load->template('produtos/formulario');
	}

	public function novo ()
	{
		$usuarioLogado = autoriza();
		
		$this->form_validation->set_rules('nome', 'nome', 'trim|required|min_length[5]|callback_nao_tenha_palavra_melhor');
		$this->form_validation->set_rules('preco', 'preço', 'required');
		$this->form_validation->set_rules('descricao', 'descrição', 'trim|required|min_length[10]');
		$this->form_validation->set_error_delimiters('<p class="alert alert-danger">', '</p>');
		$sucesso = $this->form_validation->run();

		if($sucesso) {
			$produto = array(
				'nome' => $this->input->post('nome')
			  , 'preco' => $this->input->post('preco')
			  , 'descricao' => $this->input->post('descricao')
			  , 'usuario_id' => $usuarioLogado['id']
			);

			$this->load->model('produtos_model');
			$this->produtos_model->salva($produto);

			$this->session->set_flashdata('success', 'Produto salvo com sucesso!');
			redirect('/');
		}else{
			$this->load->template('produtos/formulario');
		}
	}

	public function mostra ($id)
	{
		$this->load->model('produtos_model');
		$produto = $this->produtos_model->busca($id);

		$dados = array('produto' => $produto);

		$this->load->template('produtos/mostra', $dados);
	}

	public function nao_tenha_palavra_melhor ($nome)
	{
		$posicao = strpos($nome, "melhor");

		if($posicao !== false) {
			return true;
		}else{
			$this->form_validation->set_message('nao_tenha_palavra_melhor', "O campo '%s' não pode conter a palavra 'melhor'");
			return false;
		}
	}
}