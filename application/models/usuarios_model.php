<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios_model extends CI_Model
{
	public function buscaTodos ()
	{
		return $this->db->get('usuarios')->result_array();
	}

	public function salva ($usuario)
	{
		$this->db->insert('usuarios', $usuario);
	}

	public function buscaPorEmailESenha ($email, $senha)
	{
		$this->db->where('email', $email);
		$this->db->where('senha', $senha);
		$usuario = $this->db->get('usuarios')->row_array();

		return $usuario;
	}

	public function busca ($id)
	{
		return $this->db->get_where('usuarios', array('id' => $id))->row_array();
	}
}