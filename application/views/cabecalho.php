<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Curso Alura CodeIgniter</title>
	<link rel="stylesheet" href="<?php echo base_url('css/bootstrap.css'); ?>">
</head>
<body>
	<div class="container">
	<?php if($this->session->flashdata('success')){ ?><p class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></p><?php } ?>
	<?php if($this->session->flashdata('danger')) { ?><p class="alert alert-danger"><?php echo $this->session->flashdata('danger'); ?></p><?php } ?>