<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="<?php echo base_url('css/bootstrap.css'); ?>">
</head>
<body>
	<div class="container">
		<h1>Minhas vendas</h1>
		<table class="table">
		<?php foreach ($produtosVendidos as $produto) { ?>
		<tr>
			<td><?php echo $produto['nome']; ?></td>
			<td><?php echo dataMysqlParaPtBr($produto['data_de_entrega']); ?></td>
		</tr>
		<?php } ?>
		</table>
	</div>
</body>
</html>